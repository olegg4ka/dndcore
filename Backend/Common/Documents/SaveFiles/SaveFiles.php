<?php

namespace Core\Backend\Common\Documents\SaveFiles;


use Core\Backend\Component\MinIo\MinIo;
use Core\Component\Api\GetDocumentFromTelegram\GetDocumentFromTelegram;
use Core\Component\Container\Container;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Збереження файлу і отримання його назви як результату
 */
class SaveFiles
{
	/**
	 * @param array $data
	 * @return string
	 * @throws GuzzleException
	 */
	public static function saveFiles(array $data): string
	{
		$file = GetDocumentFromTelegram::getDocumentTelegram($data);
		$telegramId = (string)$data['telegramId'];
		$file_name = $telegramId . '/' . time() . '-' . $data['file_name'];
		$dir = '../data/files/' . $telegramId . '/';
		if (!is_dir($dir)) {
			mkdir($dir, 0777);
			chmod($dir, 0777);
		}
		(new MinIo())->saveFile(
			Container::get('config')['fileserver']['telegram']['bucketName'],
			$file,
			$file_name
		);
		return $file_name;
	}
}