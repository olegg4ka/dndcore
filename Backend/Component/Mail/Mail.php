<?php

namespace Core\Component\Mail;

use Core\Component\Container\Container;
use Exception;
use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

/**
 *
 */
class Mail
{

	private $config;
	private $client;

	/**
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->config = Container::get('config')['mail'];
		$this->client = new ApiClient('37093f57ec964665e578f540aa7ff991', 'fcfea87c21602a1b38e431e4209a82fd', new FileStorage());
	}

	/**
	 * @param $message
	 * @return void
	 */
	public function mailTo($message)
	{
		$recipients = self::getRecipient();
		$message += ['from' => array(
			'email' => $this->config['sender'],
			'name' => $this->config['senderName'],
		)];
		$message += ['to' => $recipients];
		$this->client->smtpSendMail($message);
	}

	/**
	 * @return array
	 */
	public function getRecipient(): array
	{
		$listAddressBooks = $this->client->listAddressBooks();
		$id = null;
		$countAddress = count($listAddressBooks);
		for ($i = 0; $i < $countAddress; $i++) {
			if ($listAddressBooks[$i]->name == $this->config['senderName']) {
				$id = $listAddressBooks[$i]->id;
			}
		}
		$listEmails = json_encode($this->client->getEmailsFromBook($id));
		$listEmails = json_decode($listEmails, true);
		$countEmails = count($listEmails);
		$res = [];
		for ($i = 0; $i < $countEmails; $i++) {
			$email = $listEmails[$i]['email'];
			if (isset($listEmails[$i]['variables'])) {
				$name = $listEmails[$i]['variables']['Name'];
			} else {
				$name = 'Шановни(на)';
			}
			$res[] = [
				'email' => $email,
				'name' => $name,
			];
		}
		return $res;
	}

}