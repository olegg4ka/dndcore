<?php

namespace Core\Backend\Component\User;

/**
 * Interface UserInterface
 * @package Core\Backend\Component\Auth\Users
 */
interface UserInterface
{
    /**
     * @return mixed
     */
    public function getUserID();

    /**
     * @param $userID
     * @return mixed
     */
    public function setUserID($userID);
}
