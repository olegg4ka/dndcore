<?php

namespace Core\Backend\Component\User;

class User implements UserInterface
{

	private $userID;

	/**
	 * User constructor.
	 */
	public function __construct()
	{
		if(isset($_SESSION['id'])){
			$this->userID = $_SESSION['id'];
		}else{
			$this->userID = null;
		}
	}

	/**
	 * Отримання ID користувача
	 *
	 * @return mixed
	 */
	public function getUserID()
	{
		return $this->userID;
	}

	/**
	 * Встановлення ID користувача
	 *
	 * @param mixed $userID
	 */
	public function setUserID($userID): void
	{
		$_SESSION['id'] = $userID;
		$this->userID = $userID;
	}

}