<?php

namespace Core\Component\Redis\Storage;

use Core\Component\Redis\RedisConnection;
use Core\DataBaseRequest\GetCatalog\GetCatalog;
use Exception;

/**
 * Class Commands
 * @package Core\Backend\Component\Redis\Storage
 */
class Catalog
{

	/**
	 * Отримання списку команд користувача з Redis
	 * @param $key
	 * @return array
	 * @throws Exception
	 */
	public function getCatalog($key): array
	{
		$connection = RedisConnection::getConnection(RedisConnection::CATALOG);
		$data = $connection->get($key);
		return $data !== false ? json_decode($data, true) : [];
	}

	/**
	 * Запис списку команд користувача в Redis
	 * @param $key
	 * @param array $commands
	 * @return void
	 * @throws Exception
	 */
	public function setCatalog($key, array $commands)
	{
		$connection = RedisConnection::getConnection(RedisConnection::CATALOG);
		$connection->set($key, json_encode($commands));
	}

	/**
	 * @param $key
	 * @return void
	 * @throws Exception
	 */
	public function clearCatalog($key)
	{
		$connection = RedisConnection::getConnection(RedisConnection::CATALOG);
		$connection->del($key);
	}

	/**
	 * @return void
	 * @throws Exception
	 */
	public function initCatalog()
	{
		if (!self::getCatalog('UserType')) {
			$res = GetCatalog::getRequestGetCatalog();
			$catalogs = json_decode($res[0]['getCatalog()'], true);
			$count = count($catalogs);
			for ($i = 0; $i < $count; $i++) {
				self::setCatalog($catalogs[$i]['code'], $catalogs[$i]['data']);
			}
		}
	}

}