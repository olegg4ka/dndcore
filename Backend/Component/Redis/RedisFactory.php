<?php

namespace Core\Component\Redis;

use Core\Component\Container\Container;
use Redis;

/**
 *
 */
class RedisFactory
{
	/**
	 * @return Redis
	 */
	private function createConnection(): Redis
	{
		$config = Container::get('config')['redis'];
		$connection = new Redis();
		$connection->connect(
			$config['host'],
			$config['port']
		);
		return $connection;
	}

	/**
	 * @return Redis
	 */
	public function isRedisAvailable(): Redis
	{
		if (!Container::get('config')['redis']['access']) {
			return new Redis();
		}
		return $this->createConnection();
	}

}
