<?php

namespace Core\Component\Redis;

use Exception;
use Redis;

/**
 *
 */
class RedisConnection
{

	const PERMISSION = 15;
	const CATALOG = 14;
	const APPLICATION = 13;
	const SYSTEM = 12;

	private static $connection;

	/**
	 * @param int $dbIndex
	 * @return Redis
	 * @throws Exception
	 */
	public static function getConnection(int $dbIndex): Redis
	{
		if (self::$connection) {
			self::$connection->select($dbIndex);
			return self::$connection;
		} else {
			self::$connection = (new RedisFactory())->isRedisAvailable();
			self::$connection->select($dbIndex);
			return self::$connection;
		}
	}

}