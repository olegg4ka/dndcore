<?php

namespace Core\Component\DataBase;

use Core\Backend\Utils\Checker\GetUserInfo;
use Core\Component\Container\Container;
use Core\Component\DataBase\Connection\ConnectionInterface;
use Exception;
use PDO;
use Psr\Log\LoggerInterface;

/**
 *
 */
class Executor
{
	/**
	 * @var ConnectionInterface
	 */
	private $connection;

	/**
	 * ProceduresExecutor constructor.
	 * @param ConnectionInterface $connection
	 */
	public function __construct(ConnectionInterface $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * @param string $query
	 * @param array $values
	 * @param $fetch
	 * @param string $nameDatabase
	 * @return array
	 * @throws Exception
	 */
	public function execute(string $query, array $values, $fetch = PDO::FETCH_ASSOC, string $nameDatabase = 'default_db'): array
	{
		/**
		 * @var LoggerInterface $logger
		 */
		$logger = Container::get('Logger');
		$replaceValue = [];
		foreach (array_values($values) as $value) {
			if ($value === null) {
				$replaceValue[] = $value;
				continue;
			}
			$replaceValue[] = "'" . $value . "'";
		}
		$logger->info(str_replace(array_keys($values), $replaceValue, $query));
		$connection = $this->connection->getConnection($nameDatabase);
		$db = $connection->prepare($query);
		$db->execute($values);
		if ($connection->lastInsertId()) {
			return ['id' => $connection->lastInsertId()];
		}
		return $db->fetchAll($fetch);
	}
}
