<?php

namespace Core\Component\FastRoute;

use Core\Component\Container\Container;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

/**
 * Кривий роутінг
 */
class FastRoute
{
	private $dispatcher;
	/**
	 * @var string
	 */

	/**
	 * FastRoute constructor.
	 */
	public function __construct()
	{
		$this->dispatcher = $this->setDispatcher();
	}

	/**
	 *
	 */
	private function setDispatcher()
	{
		$httpMethod = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];

		$dispatcher = simpleDispatcher(function (RouteCollector $r) use ($uri) {
			foreach (Container::get('config')['routes'] as $elem) {
				$elemPath = $elem->getPath();
				if ($uri == $elemPath) {
					$e = $elem->getDefaults();
					$r->addRoute('GET', $elemPath, (new $e['controller']())->execute());
				}

			}
		});

		if (false !== $pos = strpos($uri, '?')) {
			$uri = substr($uri, 0, $pos);
		}
		$uri = rawurldecode($uri);

		$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
		switch ($routeInfo[0]) {
			case Dispatcher::NOT_FOUND:
//				dump('404 not found7');
				break;
			case Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
//				dump($allowedMethods);
				// ... 405 Method Not Allowed
				break;
			case Dispatcher::FOUND:
				$handler = $routeInfo[1];
				$vars = $routeInfo[2];
				break;
		}
		return 0;
	}

}