<?php

namespace Core\Component\Container;

/**
 *
 */
class Container
{
	static $instance;

	/**
	 * @param object $container
	 * @return void
	 */
	public static function init(object $container)
	{
		static::$instance = $container;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public static function get($name)
	{
		return static::$instance->get($name);
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public static function set($name)
	{
		return static::$instance->set($name);
	}

}
