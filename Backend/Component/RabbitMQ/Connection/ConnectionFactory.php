<?php

namespace Core\Component\RabbitMQ\Connection;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ConnectionFactory implements ConnectionInterface
{
	private $config;
	private $connection = [];

	/**
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		$this->config = $config;
	}

	/**
	 * @param string $nameConnection
	 * @return AMQPStreamConnection
	 */
	private function createConnection(string $nameConnection): AMQPStreamConnection
	{
		$config = $this->config[$nameConnection];
		try {
			$connection = new AMQPStreamConnection($config['host'], $config['port'], $config['username'], $config['password']);
		} catch (Exception $e) {
			dump($e);
		}
		return $connection;
	}

	/**
	 * @param string $nameConnection
	 * @return mixed|AMQPStreamConnection
	 * @throws Exception
	 */
	public function getConnection(string $nameConnection = 'default_mq')
	{
		if (!isset($this->config[$nameConnection])) {
			throw new Exception('Підключення з таким ім\'ям відсутнє (nameConnection=' . $nameConnection . ')');
		}
		if (!isset($this->connection[$nameConnection])) {
			$this->connection[$nameConnection] = $this->createConnection($nameConnection);
		}

		return $this->connection[$nameConnection];
	}

}