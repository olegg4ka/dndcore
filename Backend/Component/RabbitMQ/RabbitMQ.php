<?php

namespace Core\Component\RabbitMQ;


use Core\Component\RabbitMQ\Connection\ConnectionInterface;
use Exception;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQ
{
	/**
	 * @var ConnectionInterface
	 */
	private $connection;

	/**
	 * @throws Exception
	 */
	public function __construct(ConnectionInterface $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * @param string $queueName
	 * @param string $data
	 * @param string $nameMQ
	 * @return void
	 * @throws Exception
	 */
	public function send(string $queueName, string $data, string $nameMQ = 'default_mq')
	{
		try {
			$connection = $this->connection->getConnection($nameMQ);
			$channel = $connection->channel();

			$channel->queue_declare($queueName, false, false, false, false);

			$msg = new AMQPMessage($data);
			$channel->basic_publish($msg, '', $queueName);

			$channel->close();
			$connection->close();
		} catch (Exception $e) {
			dd($e);
		}

	}

}