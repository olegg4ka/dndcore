<?php

namespace Core\Component\Api;

/**
 *
 */
class ApiFactory
{
	/**
	 * @param $request
	 * @return array
	 */
	public function rebuildResultFromApi($request): array
	{
		if ($request == null || $request == "null") {
			$request = "";
		}
		if ($request == "" || json_decode($request, true) == null) {
			$result = [];
			foreach (explode('&', urldecode($request)) as $elem) {
				$param = explode("=", $elem);
				$result[$param[0]] = $param[1];
			}
			return $result;
		}
		return json_decode($request, true);
	}
}