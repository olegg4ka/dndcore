<?php

namespace Core\Component\Api;

/**
 * Api
 */
interface Api
{
	public function execute(): string;
}
