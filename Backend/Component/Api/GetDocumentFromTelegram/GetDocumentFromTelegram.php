<?php

namespace Core\Component\Api\GetDocumentFromTelegram;

use Core\Component\Container\Container;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Отримання документу з телеграм
 */
class GetDocumentFromTelegram
{
	/**
	 * @param array $data
	 * @return string
	 * @throws GuzzleException
	 */
	public static function getDocumentTelegram(array $data): string
	{
		$client = new Client();
		$response = $client->request(
			'GET',
			'https://api.telegram.org/file/bot' . Container::get('config')['telegram']['botId'] . '/' . $data['file_path']
		);
		if ($response->getStatusCode() == 200) {
			return $response->getBody()->getContents();
		}
		return '';
	}
}