<?php

namespace Core\Backend\Component\Messenger;

use Core\Component\Container\Container;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;


/**
 * Телеграм
 */
class Telegram
{
	/**
	 * @return Api
	 * @throws TelegramSDKException
	 */
	public static function getTelegram(): Api
	{
		$config = Container::get('config');
		return new Api($config['telegram']['botId']);
	}

}