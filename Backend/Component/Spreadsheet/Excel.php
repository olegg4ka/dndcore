<?php

namespace Core\Component\Excel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Psr\Http\Message\StreamInterface;

class Excel
{
	/**
	 * @return Spreadsheet
	 */
	public function getExcel(): Spreadsheet
	{
		$file = 'test.xlsx';
		$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);; // подключить Excel-файл
		return $excel;
	}

//
//	public function __toString()
//	{
//	}
//
//	public function close()
//	{
//	}
//
//	public function detach()
//	{
//	}
//
//	public function getSize()
//	{
//	}
//
//	public function tell()
//	{
//	}
//
//	public function eof()
//	{
//	}
//
//	public function isSeekable()
//	{
//	}
//
//
//	public function seek($offset, $whence = SEEK_SET)
//	{
//	}
//
//	public function rewind()
//	{
//	}
//
//
//	public function isWritable()
//	{
//	}
//
//	public function write($string)
//	{
//	}
//
//
//	public function isReadable()
//	{
//	}
//
//
//	public function read($length)
//	{
//	}
//
//	public function getContents()
//	{
//	}
//
//	public function getMetadata($key = null)
//	{
//	}
}