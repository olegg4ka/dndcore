<?php

namespace Core\Component\Http;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

/**
 * Class Request
 * @package Core\Component\Http
 */
class Request
{
	/**
	 * @var SymfonyRequest
	 */
    private $request;

	/**
	 * Request constructor.
	 */
    public function __construct()
    {
      $this->request = SymfonyRequest::createFromGlobals();
    }

	/**
	 * @param $key
	 * @return mixed
	 */
    public function get($key)
    {
        return $this->request->get($key);
    }

	/**
	 * @return SymfonyRequest
	 */
    public function getRequest()
    {
        return $this->request;
    }

	/**
	 * @param array $keys
	 * @return string
	 */
    public function hash($keys = [])
    {
        $params = $this->getRequest()->request->all();
        $flip = array_flip($keys);
        $result = [];
        array_walk($params, function ($item, $k) use ($flip, &$result) {
            if (array_key_exists($k, $flip)) $result[$k] = (string)$item;
        });
        if (!count($result) && count($keys)) $result[] = $keys[0];
        return md5(json_encode($result, JSON_FORCE_OBJECT));
    }
}