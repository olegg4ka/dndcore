<?php

namespace Core\Backend\Component\Twig;


use Core\Component\Container\Container;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 *
 */
class Twig
{
	/**
	 * @param $data
	 * @param $name
	 * @return string
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function getTwig($data, $name): string
	{
		$config = Container::get('config');
		$loader = new \Twig\Loader\FilesystemLoader($config['twig']['path']);
		$twig = new \Twig\Environment($loader, [
			'auto_reload' => $config['twig']['auto_reload_cache'],
			'cache' => false,
		]);
		return $twig->render($name, $data);
	}

}
