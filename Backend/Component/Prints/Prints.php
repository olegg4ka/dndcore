<?php

namespace Core\Backend\Component\Prints;


use Core\Backend\Component\User\UserInterface;
use Core\Component\Container\Container;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Печатні форми
 */
class Prints
{
	/**
	 * @var
	 */
	private $userID;

	/**
	 * @var mixed
	 */
	private $config;

	/**
	 * constructor
	 */
	public function __construct()
	{
		$this->userID = Container::get(UserInterface::class)->getUserID();
		$this->config = Container::get('config')['twig'];
		$this->clearFolders();
	}

	/**
	 * Мікропечать
	 * @param $twig
	 * @param $options
	 * @return void
	 */
	public function getPdf($twig, $options)
	{
		$pdf = new Pdf;
		$pdf->setOptions($options);       // Set global PDF options (alternative)
		$pdf->addPage($twig);
		if (!is_dir($this->config['save'] . $this->userID)) {
			mkdir($this->config['save'] . $this->userID);
		}
		$pdf->saveAs($this->config['save'] . $this->userID . '/' . $this->userID . ".pdf");
	}

	/**
	 * @return void
	 */
	public function clearFolders()
	{
		if (file_exists($this->config['save'] . $this->userID)) {
			foreach (glob($this->config['save'] . $this->userID . '/*') as $file) {
				unlink($file);
			}
		}
	}

	/**
	 * @return string
	 */
	public function getUrlFile(): string
	{
		return $this->config['save'] . $this->userID . "/" . $this->userID . ".pdf";
	}

}
