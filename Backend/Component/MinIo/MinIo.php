<?php

namespace Core\Backend\Component\MinIo;

use Core\Component\Container\Container;

/**
 * FileServer
 */
class MinIo
{
	private $s3;

	private $config;

	private $fileSize;

	/**
	 *
	 */
	public function __construct()
	{
		$this->config = Container::get('config')['fileserver']['telegram'];
		$this->s3 = $this->createConnection();
	}

	/**
	 * @return void
	 */
	public function saveFile(string $bucket, $file, $file_name)
	{

		$policyReadOnly = '{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetBucketLocation",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Resource": [
        "arn:aws:s3:::%s"
      ],
      "Sid": ""
    },
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Resource": [
        "arn:aws:s3:::%s/*"
      ],
      "Sid": ""
    }
  ]
}
';

		$buckets = $this->s3->listBuckets([])->toArray()['Buckets'];
		$names = [];
		foreach ($buckets as $elem) {
			array_push($names, $elem['Name']);
		}

		if (!in_array($bucket, $names)) {
			$result = $this->s3->createBucket([
				'Bucket' => $bucket,
			]);
			$this->s3->putBucketPolicy([
				'Bucket' => $bucket,
				'Policy' => sprintf($policyReadOnly, $bucket, $bucket),
			]);
		}
		$insert = $this->s3->putObject([
			'Bucket' => $bucket,
			'Key' => $file_name,
			'Body' => $file
		]);
	}

	/**
	 * @param string $bucket
	 * @param $file_name
	 * @return void
	 */
	public function getFile(string $bucket, $file_name)
	{
		$retrive = $this->s3->getObject([
			'Bucket' => $bucket,
			'Key' => $file_name,
			'SaveAs' => '../data/files/' . $file_name
		]);
		chmod('../data/files/' . $file_name, 0777);
		$this->fileSize = $retrive->toArray()['ContentLength'];
	}

	/**
	 * @return int
	 */
	public function getFileSize(): int
	{
		return (int)$this->fileSize;
	}

	/**
	 * @return \Aws\S3\S3Client
	 */
	private function createConnection(): \Aws\S3\S3Client
	{
		return new \Aws\S3\S3Client([
			'version' => 'latest',
			'region' => 'us-east-1',
			'endpoint' => $this->config['url'],
			'use_path_style_endpoint' => true,
			'credentials' => [
				'key' => $this->config['key'],
				'secret' => $this->config['secret'],
			],
		]);
	}

	/**
	 * @return \Aws\S3\S3Client
	 */
	public function getConnection(): \Aws\S3\S3Client
	{
		return $this->s3;
	}

}