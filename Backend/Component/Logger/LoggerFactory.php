<?php


namespace Core\Component\Logger;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class LoggerFactory implements LoggerFactoryInterface
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var array
	 */
	private $loggers = [];

	/**
	 * @var
	 */
	private $path;

	/**
	 * LoggerFactory constructor.
	 * @param ContainerInterface $container
	 * @param LoggerInterface $logger
	 */
	public function __construct(ContainerInterface $container, LoggerInterface $logger){
		$this->container = $container;
		$this->logger = $logger;
	}

	/**
	 * @param string $name
	 * @param string $handler
	 * @return LoggerInterface
	 */
	public function createLogger(string $name, string $handler = 'stream'): LoggerInterface
	{
		if (!isset($this->loggers['name'])){
			switch ($handler){
				case 'stream':
				default:
					$log = new StreamHandler($this->container->get('config')['logger']['stream']['path'] . $this->path . $name . '.log', Logger::INFO);
					$log->setFormatter(new LineFormatter());
					$this->logger->pushHandler($log);
			}
			$this->loggers[$name] = $this->logger;
		}
		return $this->loggers[$name];
	}

	/**
	 * @param string $name
	 * @return bool|null
	 */
	public function getLogger(string $name): ?bool
	{
		return isset($this->loggers['name']) ?? null;
	}

	/**
	 * @param string $path
	 * @return $this
	 */
	public function setPath(string $path): LoggerFactory
	{
		$this->path = $path;
		return $this;
	}

}