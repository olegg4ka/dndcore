<?php

use Core\Backend\Component\User\UserInterface;
use Core\Component\Container\Container;
use Core\Component\Logger\LoggerFactory;
use Core\Component\Logger\LoggerFactoryInterface;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return [
	LoggerInterface::class => DI\factory(function (ContainerInterface $container) {
		return new Logger($container->get('config')['logger']['name']);
	}),
	LoggerFactoryInterface::class => DI\factory(function (LoggerFactory $loggerFactory) {
		return $loggerFactory;
	}),
	'Logger' => DI\factory(function (ContainerInterface $container, LoggerFactoryInterface $loggerFactory) {
		$userId = Container::get(UserInterface::class)->getUserID();

		return $loggerFactory
			->setPath(date('d.m.Y') . '/' . $userId . '/')
			->createLogger('logs');
	}),
	'LoggerStartApp' => DI\factory(function (ContainerInterface $container, LoggerFactoryInterface $loggerFactory) {
		return $loggerFactory
			->setPath(date('d.m.Y') . '/err/')
			->createLogger('err_start_app_logs');
	})
];
