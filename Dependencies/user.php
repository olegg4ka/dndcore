<?php


use Core\Backend\Component\User\User;
use Core\Backend\Component\User\UserInterface;
use Psr\Container\ContainerInterface;

return [
    UserInterface::class => DI\factory(function (ContainerInterface $container) {
        return new User;
    })
];
