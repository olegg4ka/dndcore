<?php


use Core\Component\DataBase\Connection\ConnectionFactory;
use Core\Component\DataBase\Connection\ConnectionInterface;
use Psr\Container\ContainerInterface;

return [
    ConnectionInterface::class =>  DI\factory(function (ContainerInterface $container){
        return new ConnectionFactory($container->get('config')['DataBase']);
    }),
    'ConnectDatabase' => DI\factory(function (ConnectionInterface $connection): ConnectionFactory {
        return $connection;
    })
];
