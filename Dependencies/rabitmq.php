<?php


use Core\Component\RabbitMQ\Connection\ConnectionFactory;
use Core\Component\RabbitMQ\Connection\ConnectionInterface;
use Psr\Container\ContainerInterface;

return [
    ConnectionInterface::class =>  DI\factory(function (ContainerInterface $container){
        return new ConnectionFactory($container->get('config')['RabbitMq']);
    }),
    'ConnectRabbitMq' => DI\factory(function (ConnectionInterface $connection): ConnectionFactory {
        return $connection;
    })
];
