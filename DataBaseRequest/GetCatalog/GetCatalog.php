<?php

namespace Core\DataBaseRequest\GetCatalog;

use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 * Довідники
 */
class GetCatalog
{
	/**
	 * Запит отримання всіх довідників
	 * @return array
	 */
	public static function getRequestGetCatalog(): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("SELECT getCatalog()",
			[],
			2,
			'default_db'
		);
	}

}