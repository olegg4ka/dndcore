<?php

namespace Core\DataBaseRequest\CheckUser;


use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 * Запит по перевірці юзерів
 */
class CheckUser
{
	/**
	 * @param array $message
	 * @return array
	 */
	public static function checkUser(array $message): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("CALL checkuser(:data)",
			[
				':data' => json_encode($message),
			],
			2,
			'default_db');
	}

}