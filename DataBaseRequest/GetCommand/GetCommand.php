<?php

namespace Core\DataBaseRequest\GetCommand;

use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 * Запит по командам
 */
class GetCommand
{
	/**
	 * Метода виконання логіки по командам
	 * @param string $command
	 * @param string $param
	 * @param array $message
	 * @return array
	 */
	public static function getRequestCommand(string $command, string $param, array $message): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("CALL getCommand(:command, :param, :data)",
			[
				':command' => $command,
				':param' => $param,
				':data' => json_encode($message),
			],
			2,
			'default_db'
		);
	}

}